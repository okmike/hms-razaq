package views;

import views.listener.LoginButtonClickedListener;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class Login {
    private JButton buildButton(String text){
        JButton button = new JButton(text);
        button.setPreferredSize(new Dimension(150, 20));
        return button;
    }

    public JPanel mainPanel;
    private JTextField employeeID;
    private JPasswordField passwordField;
    public JButton loginButton;

    public Login(){
        mainPanel = new JPanel();
        mainPanel.setBorder(new EmptyBorder(10,10,10,10));
        GridLayout layout = new GridLayout(4,0,100,100);
        JPanel left = new JPanel(layout);

        loginButton = buildButton("Login");
        employeeID = new JTextField("Employee ID");
        passwordField = new JPasswordField("Password");

        left.add(employeeID);
        left.add(passwordField);
        left.add(loginButton);

        mainPanel.add(left, BorderLayout.WEST);
    }

    public static JPanel render(JFrame owner){
        Login loginWindow = new Login();
        loginWindow.loginButton.addActionListener(new LoginButtonClickedListener(owner));
        return loginWindow.mainPanel;
    }
}
