package views;


import javax.swing.*;
import java.awt.*;

public class StaffDashboard {
    private JPanel mainPanel;

    public StaffDashboard(){
        if(mainPanel == null){
            mainPanel = new JPanel();
        }
    }

    public static JPanel render(JFrame owner){
        StaffDashboard dashboard = new StaffDashboard();
        GridLayout layout = new GridLayout(0, 2); //unlimited rows, 2 columns
        dashboard.mainPanel.setLayout(layout);

        JButton recentPatientsButton = new JButton("7 Patients");
        JButton recentDrugsButton = new JButton("5 Drugs");
        JButton recentDiagButton = new JButton("3 Diagnosis");
        JButton recentPrescButton = new JButton("4 Prescriptions");

        dashboard.mainPanel.add(recentPatientsButton);
        dashboard.mainPanel.add(recentDiagButton);
        dashboard.mainPanel.add(recentDrugsButton);
        dashboard.mainPanel.add(recentPrescButton);

        return dashboard.mainPanel;
    }
}
