package views;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;

public class Main extends JFrame{
    public static final String APP_NAME = "ZAQ Health Management System";

    private static Main mainComponent;

    private Main(){

    }

    public static Main getInstance(){
        Main instance = new Main();
        instance.setTitle(APP_NAME);
        return instance;
    }

    private void initUI(){
        createMenuBar();
        setTitle(APP_NAME);
        setSize(800, 500);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    private void createMenuBar(){
        JMenuBar menuBar = new JMenuBar();

        menuBar.add(buildFileMenu());
        menuBar.add(buildEditMenu());
        menuBar.add(buildToolsMenu());

        setJMenuBar(menuBar);
    }

    private JMenu buildFileMenu(){
        JMenu file = new JMenu("File");
        file.setMnemonic(KeyEvent.VK_F);

        JMenu newItem = new JMenu("New");
        newItem.setMnemonic(KeyEvent.VK_N);
        //assemble sub menus of new
        JMenuItem newDrugMenuItem  = new JMenuItem("Drug");
        JMenuItem newPatientMenuItem  = new JMenuItem("Patient");
        JMenuItem newEmployeeMenuItem  = new JMenuItem("Employee");
        JMenuItem newDiagnosisMenuItem  = new JMenuItem("Diagnosis");

        /*newPatientMenuItem.addActionListener(new NewBookClickedListener(this));
        newEmployeeMenuItem.addActionListener(new NewAuthorClickedListener(this));
        newDrugMenuItem.addActionListener(new NewStudentClickedListener(this));
        issueSubNew.addActionListener(new NewIssueClickedListener(this));
        newDiagnosisMenuItem.addActionListener(new NewPublisherClickedListener(this));*/

        newItem.add(newPatientMenuItem);
        newItem.add(newEmployeeMenuItem);
        newItem.add(newDiagnosisMenuItem);
        newItem.add(newDrugMenuItem);
        file.add(newItem);

        JMenu listItem = new JMenu("List");
        listItem.setMnemonic(KeyEvent.VK_L);

        JMenuItem listDrugsMenuItem = new JMenuItem("Drugs");
        JMenuItem listPatientsMenuItem = new JMenuItem("Patients");
        JMenuItem listDiagnosisMenuItem = new JMenuItem("Diagnosis");
        JMenuItem listEmployeesMenuItem = new JMenuItem("Employees");

        /*listDiagnosisMenuItem.addActionListener(new ListAuthorsClickedListener(this));
        issuesSubList.addActionListener(new ListIssuesClickedListener(this));
        listDrugsMenuItem.addActionListener(new ListBooksClickedListener(this));*/

        listItem.add(listDrugsMenuItem);
        listItem.add(listPatientsMenuItem);
        listItem.add(listDiagnosisMenuItem);
        listItem.add(listEmployeesMenuItem);

        file.add(listItem);

        JMenuItem viewStudentRecordItem = new JMenuItem("Patient's Record");
        file.add(viewStudentRecordItem);

        file.addSeparator();

        JMenuItem exitItem = new JMenuItem("Exit");
        /*exitItem.addActionListener(new ExitClickedListener());*/
        file.add(exitItem);

        return file;
    }

    private JMenu buildToolsMenu(){
        JMenu tools = new JMenu("Tools");
        tools.setMnemonic(KeyEvent.VK_T);

        JMenuItem statsItem = new JMenuItem("Statistics");
        JMenuItem adminItem = new JMenuItem("Administration");
        JMenuItem logoutItem = new JMenuItem("End Session");

        tools.add(statsItem);
        tools.add(adminItem);
        tools.add(logoutItem);

        return tools;
    }

    private JMenu buildEditMenu(){
        JMenu edit = new JMenu("Update Info");
        edit.setMnemonic(KeyEvent.VK_E);

        JMenuItem updateDrugItem = new JMenuItem("Drug");
        JMenuItem updatePatientItem = new JMenuItem("Patient");
        JMenuItem  updateDiagnosisItem = new JMenuItem("Diagnosis");
        JMenuItem updateEmployeeItem = new JMenuItem("Employee");
        /*studentItem.addActionListener(new MenuEditStudentListener(this));*/
        //todo set mnemonics for above

        edit.add(updateDrugItem);
        edit.add(updatePatientItem);
        edit.add(updateDiagnosisItem);
        edit.add(updateEmployeeItem);

        return edit;
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                mainComponent = Main.getInstance();
                mainComponent.initUI();
                mainComponent.getContentPane().removeAll();
                mainComponent.getContentPane().add(Login.render(mainComponent));
                mainComponent.setVisible(true);
            }
        });
    }

    /**
     * updates the main window by removing current items and replacing them with the parameter
     * @param component the new component to use as replacement
     */
    public static void updateMainUI(JComponent component){
        mainComponent.getContentPane().removeAll();
        mainComponent.getContentPane().add(component);
        mainComponent.setVisible(true);
    }
}
