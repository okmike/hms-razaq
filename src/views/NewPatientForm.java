package views;

import javax.swing.*;


public class NewPatientForm {
    private JLabel surnameLabel;
    private JTextField surnameField;
    private JLabel firstNameLabel;
    private JTextField firstNameField;
    private JLabel phoneNumberLabel;
    private JTextField phoneNumberField;
    private JLabel genderLabel;
    private JComboBox genderField;
    private JPanel mainPanel;

    public NewPatientForm(){

    }

    public static void buildInsertUI(JFrame owner){
        JDialog frame = new JDialog(owner, "Add new Patient");
        frame.setContentPane(new NewPatientForm().mainPanel);
        frame.pack();
        frame.setLocationRelativeTo(owner);
        frame.setSize(450, 200);
        frame.setVisible(true);
    }
}
