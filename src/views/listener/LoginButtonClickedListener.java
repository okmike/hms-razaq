package views.listener;

import views.NewPatientForm;
import views.Main;
import views.StaffDashboard;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoginButtonClickedListener implements ActionListener {
    private JFrame dialogOwner; //if a dialog is to be shown, it should be a child of this frame

    public LoginButtonClickedListener(JFrame owner){
        dialogOwner = owner;
    }

    public void actionPerformed(ActionEvent event){
        Main.updateMainUI(StaffDashboard.render(dialogOwner));
    }
}
