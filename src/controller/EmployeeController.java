package controller;


public class EmployeeController {
    private String loggedEmployeeID;
    private String loggedEmployeePass;

    private EmployeeController(){

    }

    public boolean actionLogin(String empID, String pass){
        //implementation should fetch data from dao, if login succeeded
        //todo do some actual login
        return true;
    }

    public static EmployeeController getInstance(){
        return new EmployeeController();
    }
}
