package helpers;


import javax.swing.*;
import java.sql.Connection;
import java.sql.DriverManager;

public class Connector {
    private static final String DB_USERNAME = "root";
    private static final String DB_PASSWORD = "asdf";
    private static final String DB_URL = "jbdc:mysql://localhost/hms-razaq";

    public static Connection getConnection(){
        try{
            Class.forName("com.mysql.jbdc.Driver");
            return DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
        }
        catch (Exception up){
            JOptionPane.showMessageDialog(null, "Failed to connect database");
            return null;
        }
    }
}
